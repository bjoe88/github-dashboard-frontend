import { Component, OnInit } from '@angular/core';
import { SearchService } from '../shared/provider/search/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  query = '';
  language = '';
  topic = '';

  showResult = false;
  repos: any = [];
  afterRepo = '';
  beforeRepo = '';

  constructor(private searchSerivce: SearchService) { }

  ngOnInit() {
  }

  async search(after = '', before = '') {
    const s = this;
    s.showResult = true;
    s.repos = [];
    const data = await s.searchSerivce.search(s.query, s.language, s.topic, after, before);
    s.repos = data[0];
    s.afterRepo = data[1].endCursor;
    s.beforeRepo = data[1].startCursor;
  }

  next() {
    this.search(this.afterRepo);
  }

  prev() {
    this.search('', this.beforeRepo);
  }
}
