import { Component, OnInit } from '@angular/core';
import { SearchService } from '../shared/provider/search/search.service';
import { Repo } from '../shared/model/repo.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {
  searchResults: any = [];
  constructor(private searchSerivce: SearchService) {

  }

  ngOnInit() {
    const s = this;
    s.searchSerivce.getSearchRecord()
      .then((data) => {
        s.searchResults = data;
        s.searchResults.map((ele) => {
          const repos = JSON.parse(ele.data);
          if (!!repos.data && !!repos.data.search) {
            const repos2: Repo[] = repos.data.search.edges.map((edge: any) => {
              return new Repo().deserializeRepo(edge);
            });
            ele.repos = repos2;

          }
        });
      });
  }
}
