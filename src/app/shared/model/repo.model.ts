import { Language } from './language.model';
import { Topic } from './topic.model';
export class Repo {
    id: number;
    name: string;
    description: string;
    language: Language;
    topics: Topic[];

    deserializeRepo(input: any): Repo {
        const s = this;
        s.id = input.node.id;
        s.name = input.node.name;
        s.description = input.node.descriptionHTML;
        if (!!input.node.primaryLanguage) {
            s.language = new Language().deserializeLanguage(input.node.primaryLanguage);
        }

        s.topics = input.node.repositoryTopics.edges.map((topic: any) => {
            return new Topic().deserializeTopic(topic);
        });
        return s;

    }
}
