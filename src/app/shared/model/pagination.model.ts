export class Pagination {
    startCursor: string;
    endCursor: string;
    hasPreviousPage: boolean;
    hasNextPage: boolean;

    deserializePagination(input: any): Pagination {
        const s = this;
        s.startCursor = input.startCursor;
        s.endCursor = input.endCursor;
        s.hasPreviousPage = input.hasPreviousPage;
        s.hasNextPage = input.hasNextPage;
        return s;
    }
}
