export class Topic {
    id: number;
    name: string;
    description: string;

    deserializeTopic(input: any): Topic {
        const s = this;
        s.id = input.node.id;
        s.name = input.node.topic.name;
        return s;
    }
}
