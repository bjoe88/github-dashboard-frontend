
export class Language {
    name: string;
    color: string;

    deserializeLanguage(input: any): Language {
        const s = this;
        s.name = input.name;
        s.color = input.color;
        return s;
    }
}
