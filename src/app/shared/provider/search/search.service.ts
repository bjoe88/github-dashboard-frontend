import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Repo } from '../../model/repo.model';
import { Pagination } from '../../model/pagination.model';
import { environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  async search(query: string, language = '', topic = '', after = '', before = '') {
    const s = this;
    const params: any = {};
    params.q = query;
    if (language !== '') {
      params.language = language;
    }
    if (topic !== '') {
      params.topic = topic;
    }
    if (after !== '') {
      params.after = after;
    }
    if (before !== '') {
      params.before = before;
    }
    return new Promise((resolve, reject) => {
      s.http.get(environment.apiUrl, {
        params
      })
        .subscribe({
          next(res: any) {
            if (!!res.data && !!res.data.search) {
              const repos: Repo[] = res.data.search.edges.map((edge: any) => {
                return new Repo().deserializeRepo(edge);
              });
              const pagination = new Pagination().deserializePagination(res.data.search.pageInfo);
              resolve([repos, pagination]);
            } else {
              console.log('Server Error');
            }
          }
        });
    });
  }

  async getSearchRecord() {
    return new Promise((resolve, reject) => {
      const s = this;
      const params: any = {};
      s.http.get(environment.apiUrl + '/get-search-record', {
        params

      })
        .subscribe({
          next(res: any) {
            resolve(res);
          }
        });
    });
  }
}
